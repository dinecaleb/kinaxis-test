import { Component, OnDestroy, Input, ViewChild } from '@angular/core';
import { NbThemeService, NbColorHelper } from '@nebular/theme';
import { DataImportService } from '../../../@core/mock/data-import.service';
import { ChartComponent } from 'angular2-chartjs';
@Component({
  selector: 'ngx-chartjs-line',
  templateUrl: './chartjs-line.component.html',
  styleUrls: ['./chartjs.component.scss'],
})
export class ChartjsLineComponent implements OnDestroy {
  @ViewChild('chart1') chart: ChartComponent;
  data: any = {
    labels: '',
    datasets: []
  };
  summary: any;
  options: any;
  total: string;
  themeSubscription: any;
  selectedItem = 12;
  selectedChart: string = 'bar'
  selectedWeek: number = 0;
  selectedMonth: any;
  sumProps = prop => (sum, obj) => sum += obj[prop];
  dateRange: string = 'All 2015';
  allData: any;
  charts = [{ name: "Bar Chart", value: 'bar' }, { name: "Line Chart", value: 'line' }]
  timeline = [{ name: "All data", value: 12 }, { name: "January", value: 0 }, { name: "February", value: 1 }, { name: "March", value: 2 }, { name: "April", value: 3 }, { name: "May", value: 4 }, { name: "June", value: 5 },
  { name: "July", value: 6 }, { name: "August", value: 7 }, { name: "September", value: 8 }, { name: "October", value: 9 }, { name: "November", value: 10 }, { name: "December", value: 11 }
  ];

  months: Array<string> = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];

  weeks: Array<Object>;


  constructor(private theme: NbThemeService, private importService: DataImportService) {

  }

  forceChartRefresh() {
    setTimeout(() => {
      this.chart.chart.update();
    }, 10);
  }


  ngOnInit() {
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {

      this.importService.allData.subscribe(data => {
        this.selectedItem = 12;
        this.selectedChart = 'bar'
        this.selectedWeek = 0;
        this.allData = data;
        const colors: any = config.variables;
        const chartjs: any = config.variables.chartjs;

        this.data = {
          labels: this.allData.labels,
          datasets: this.allData.data,
        };
        this.summary = this.allData.total;
        this.total = this.findTotal(this.summary);

        this.options = {
          responsive: true,
          maintainAspectRatio: false,
          scales: {
            xAxes: [
              {
                gridLines: {
                  display: true,
                  color: chartjs.axisLineColor,
                },
                ticks: {
                  fontColor: chartjs.textColor,
                },
                scaleLabel: {
                  display: true,
                  labelString: "Time scale",
                  fontColor: 'white',
                  fontSize: 16
                },
              },
            ],
            yAxes: [
              {
                gridLines: {
                  display: true,
                  color: chartjs.axisLineColor,
                },
                ticks: {
                  fontColor: chartjs.textColor,
                },
                scaleLabel: {
                  display: true,
                  labelString: 'Number of flights',
                  fontColor: 'white',
                  fontSize: 16
                }
              },
            ],
          },
          tooltips: {
            bodyFontSize: 15,
            titleFontSize: 16,
            bodySpacing: 8,
          },
          legend: {
            labels: {
              fontColor: chartjs.textColor,
              padding: 20,
              boxWidth: 12,
              fontStyle: 'bold'
            },
          },
          title: {
            display: true,
            position: 'top',
            text: 'Airlines',
            fontColor: 'white',
            fontSize: 16
          }
        };
      });
    })
  }

  onChangeChart(selected) {
    this.selectedChart = selected; 
    const week = this.selectedWeek;
    this.onChangeMonth(this.selectedMonth); 
    this.selectedWeek = week;
    this.onChangeWeek(week);


  }

  onChangeMonth(selected) {
    let month; let lastDay;
    this.selectedWeek = 0;
    if(selected != 12){
      month = this.months[selected];
       lastDay = this.importService.getLastDay(selected);
      if(lastDay != 0){
        this.weeks  = [{ name: month + " 1 - "+ month + " 7" , value: 1 }, { name: month + " 8 - "+ month + " 14" , value: 2 }, 
      { name: month + " 15 - "+ month + " 21" , value: 3 }, { name: month + " 22 - "+ month + " 28" , value: 4 }, 
      { name: month + " 29 - "+ month + " "+lastDay , value: 5 },
      { name: "All weeks", value: 6 }];
      }
      else{
        this.weeks = ["No data"];
        this.dateRange = "No data"
      }
    }
    if (selected == 12) {
      this.dateRange = 'All 2015 data';

      this.importService.allData.subscribe(data => {
        this.data.labels = data.labels;
        this.data.datasets = data.data;
        this.summary = data.total;
        this.total = this.findTotal(this.summary);
        this.forceChartRefresh()
      })
    }
    if (selected == 0) {
      this.monthFilter(selected);
    }
    if (selected == 1) {
      this.weeks  = [{ name: month + " 1 - "+ month + " 7" , value: 1 }, { name: month + " 8 - "+ month + " 14" , value: 2 }, 
      { name: month + " 15 - "+ month + " 21" , value: 3 }, { name: month + " 22 - "+ month + " 28" , value: 4 },
      { name: "All weeks", value: 6 }];
      this.monthFilter(selected)
    }
    if (selected == 2) {
      this.monthFilter(selected)
    }
    if (selected == 3) {
      this.monthFilter(selected)
    }
    if (selected == 4) {
      this.monthFilter(selected)
    }
    if (selected == 5) {
      this.monthFilter(selected)
    }
    if (selected == 6) {
      this.monthFilter(selected)
    }
    if (selected == 7) {
      this.monthFilter(selected)
    }
    if (selected == 8) {
      this.monthFilter(selected)
    }
    if (selected == 9) {
      this.monthFilter(selected)
    }
    if (selected == 10) {
      this.monthFilter(selected)
    }
    if (selected == 11) {
      this.monthFilter(selected)
    }

  }

  findTotal(array){
      var size = 0;
    
      for (var i = 0; i < array.length; i++) {
        let num =  array[i].size;
        if(/[,\-]/.test(num)){
         num = array[i].size.replace(/\,/g,'')
        }
        
        num=Number(num)
        size += num ;
      }
      return size.toLocaleString();
  }

  onChangeWeek(selected) {

    if (selected == 1) {
      this.weekFilter(1,7)
    }
    if (selected == 2) {
      this.weekFilter(8,14)
    }
    if (selected == 3) {
      this.weekFilter(15,21)
    }
    if (selected == 4) {
      this.weekFilter(22,28)
    }
    if (selected == 5) {
      const lastDay = this.importService.getLastDay(this.selectedMonth);
      this.weekFilter(29,lastDay);
    }

    if (selected == 6) {
      this.onChangeMonth(this.selectedMonth);
    }

  }

  monthFilter(monthNum) {
    const month = this.importService.getAirlineDataByMonth(monthNum,this.selectedChart);
    this.selectedMonth = monthNum;
    this.dateRange = this.months[monthNum];
    this.data.labels = month.labels;
    this.data.datasets = month.data;
    this.summary = month.total;
    this.total = this.findTotal(this.summary);

    this.forceChartRefresh()
  }

  weekFilter(from,to) {
    const week = this.importService.getAirlineDataByWeek(this.selectedMonth, from, to,this.selectedChart)
    this.dateRange = this.months[this.selectedMonth]  + " " + from + " - " + this.months[this.selectedMonth] + " " + to;
    this.data.labels = week.labels;
    this.data.datasets = week.data;
    this.summary = week.total;
    this.total = this.findTotal(this.summary);
    this.forceChartRefresh()
  }


  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }
}
