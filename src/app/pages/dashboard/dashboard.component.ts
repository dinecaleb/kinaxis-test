import {Component, OnDestroy} from '@angular/core';
import { DataImportService } from '../../@core/mock/data-import.service';
import data from './flights_mini.json';

@Component({
  selector: 'ngx-dashboard',
  styleUrls: ['./dashboard.component.scss'],
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnDestroy {

  csvData:any = new Array();
  loading = false;

  constructor(private importService: DataImportService) {
   
  }

  toggleLoadingAnimation() {
    this.loading = true;
    setTimeout(() => this.loading = false, 10000);
  }

  

  onUpload(event: {type: string, data: any}) {
    if (event.type === 'success') {
      this.loading = false
      this.csvData = this.importService.getAllData(event.data);
    } else { 
      setTimeout(() => this.loading = false, 3000);
      console.log(event.data); // error
    }
  
  }
  ngOnDestroy() {
   
  }
}
