import { NgModule } from '@angular/core';

import { NgxEchartsModule } from 'ngx-echarts';

import { ThemeModule } from '../../@theme/theme.module';
import { DashboardComponent } from './dashboard.component';
import { CSV2JSONModule } from 'angular2-csv2json';
import { NbSpinnerModule } from '@nebular/theme';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ChartModule } from 'angular2-chartjs';
import { ChartjsLineComponent } from '../charts/chartjs/chartjs-line.component';


const components = [
  ChartjsLineComponent,
];


@NgModule({
  imports: [
    ThemeModule,
    NgxEchartsModule,
    CSV2JSONModule,
    NbSpinnerModule, NgxEchartsModule, NgxChartsModule, ChartModule
  ],
  declarations: [
    DashboardComponent, ...components
  ],
})
export class DashboardModule { }
