
export interface Barchart {
    label: string
    data: Array<number>
    backgroundColor?: string
    borderColor?: string
    pointRadius?: any
}
