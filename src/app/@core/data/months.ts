import { Flight } from './flight';

export interface Months {
    name: string
    data: Array<Flight>
}
