
export interface Flight {
    AIRLINE: string
    AIR_TIME: number
    ARRIVAL_DELAY: number
    ARRIVAL_TIME: number
    CANCELLATION_REASON: string
    CANCELLED: number
    DAY: number
    DAY_OF_WEEK: number
    DEPARTURE_DELAY: number
    DEPARTURE_TIME: Number
    DESTINATION_AIRPORT: string
    DISTANCE: number
    MONTH: number
    ORIGIN_AIRPORT: string
    SCHEDULED_ARRIVAL: string
    SCHEDULED_DEPARTURE: string
}
