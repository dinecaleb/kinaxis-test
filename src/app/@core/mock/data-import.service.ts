import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { Flight } from '../data/flight';
import { Months } from '../data/months';
import { Barchart } from '../data/barChart';
import { NbColorHelper } from '@nebular/theme';
import 'rxjs/add/observable/of';
import { chartData } from '../data/data';


@Injectable()
export class DataImportService {

    airlines: Array<Barchart>;
    labels: any = new Array();
    rawData: any;
    _allData = new BehaviorSubject<any>(0);
    allData = this._allData.asObservable();
    sortedData: Array<Months>;
    months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    ];
    days: Array<string> = ["Mon", "Tues", "Wed", "Thu", "Fri", "Sat",
        "Sun"]

    constructor() { }

    getAirlines(data) {
        let airlines: Array<string> = new Array();
        data.forEach((element: Flight) => {
            airlines.push(element.AIRLINE);
            //  this.sortMonths(element, months);

        });
        airlines = Array.from(new Set(airlines)).filter(function (e) { return e.replace(/(\r\n|\n|\r)/gm, "") });
        const airlineList = new Array();
        airlines.forEach(element => {
            const airline: Barchart = {
                label: element,
                data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            }
            airlineList.push(airline);
        });
        return airlineList;

    }

    getDataByMonth(month, data) {
        const monthData = new Array();
        data.forEach((element: Flight) => {
            if (element.MONTH == month) {
                monthData.push(element);
            }
        });
        return monthData;
    }

    getAllData(data) {
        this.rawData = data;
        const months = this.initMonths();
        this.sortedData = this.sortMonths(data, months);
        const dataByYear = this.getAirlineDataByYear(this.sortedData);
        this._allData.next(dataByYear);
        return dataByYear;

    }

    getAirlineDataByYear(data: Months[]) {
        
        this.airlines = this.getAirlines(this.rawData);
        data.forEach((month, index) => {
            month.data.forEach((flight: Flight) => {

                this.airlines.forEach((airline) => {
                    if (airline.label == flight.AIRLINE) {
                        airline.data[index] = airline.data[index] + 1;
                    }
                });
           
            });
            // total.total[index] = month.data.length;
            month["size"] =  month.data.length.toLocaleString(); 
      
        });

        this.labels = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
        ];
       
        this.airlines.forEach((airline,index) => {
            const color = this.genColor();
            airline.backgroundColor = NbColorHelper.hexToRgbA(color, 0.8);
            airline.borderColor = color;
            airline.pointRadius = 3;
        });

        const yearlyData: chartData = {
            data: this.airlines,
            labels: this.labels,
            total: data,
        }
        return yearlyData;
        // const airlines
    }


    getAirlineDataByMonth(month, chartType) {
        this.airlines = this.getAirlines(this.rawData);
        const total = new Array(7).fill(0);
        this.sortedData[month].data.forEach((flight: Flight) => {

            this.airlines.forEach((airline) => {
                if (airline.label == flight.AIRLINE && flight.DAY_OF_WEEK == 1) {
                    airline.data[0] = airline.data[0] + 1;
                }
                if (airline.label == flight.AIRLINE && flight.DAY_OF_WEEK == 2) {
                    airline.data[1] = airline.data[1] + 1;
                }
                if (airline.label == flight.AIRLINE && flight.DAY_OF_WEEK == 3) {
                    airline.data[2] = airline.data[2] + 1;
                }
                if (airline.label == flight.AIRLINE && flight.DAY_OF_WEEK == 4) {
                    airline.data[3] = airline.data[3] + 1;
                }
                if (airline.label == flight.AIRLINE && flight.DAY_OF_WEEK == 5) {
                    airline.data[4] = airline.data[4] + 1;
                }
                if (airline.label == flight.AIRLINE && flight.DAY_OF_WEEK == 6) {
                    airline.data[5] = airline.data[5] + 1;
                }
                if (airline.label == flight.AIRLINE && flight.DAY_OF_WEEK == 7) {
                    airline.data[6] = airline.data[6] + 1;
                }
             

            });
            if( flight.DAY_OF_WEEK == 1){

                total[0] =  total[0] +1;
            }
            if( flight.DAY_OF_WEEK == 2){
                total[1] =  total[1] +1;
            }
            if( flight.DAY_OF_WEEK == 3){
                total[2] =  total[2] +1;
            }
            if( flight.DAY_OF_WEEK == 4){
                total[3] =  total[3] +1;
            }
            if( flight.DAY_OF_WEEK == 5){
                total[4] =  total[4] +1;
            }
            if( flight.DAY_OF_WEEK == 6){
                total[5] =  total[5] +1;
            }
            if( flight.DAY_OF_WEEK == 7){
                total[6] =  total[6] +1;
            }
        });


        this.labels = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday",
            "Sunday"];

        total.forEach((data,index)=>{
          total[index] = {
            name : this.labels[index],
            size : data,
          }
        })
        let opacity = 0.8;
        if (chartType == 'line') {
            opacity = 0.3;
        }
        this.airlines.forEach((airline) => {
            const color = this.genColor();
            airline.backgroundColor = NbColorHelper.hexToRgbA(color, opacity);
            airline.borderColor = color;
            airline.pointRadius = 3;
        });

        const monthlyData: chartData = {
            data: this.airlines,
            labels: this.labels,
            total: total
        }
        return monthlyData;
        // const airlines
    }


    getLastDay(month){
        let lastDay = Number(this.sortedData[month].data[this.sortedData[month].data.length-1].DAY); 
        if(!this.sortedData[month]){
           lastDay = 0
        }
       
        return lastDay;
    }
    getAirlineDataByWeek(month, from, to, chartType) {
        this.airlines = this.getAirlines(this.rawData);
        let lastDay: number = 0;
        let firstDay: number;
        const total = new Array(7).fill(0);
        this.sortedData[month].data.forEach((flight: Flight) => {
            this.airlines.forEach((airline) => {
                if (airline.label == flight.AIRLINE && flight.DAY_OF_WEEK == 1 && flight.DAY >= from && flight.DAY <= to) {
                    const day = this.checkDays(Number(flight.DAY) -1);
                    airline.data[day] = airline.data[day] + 1;
                }
                if (airline.label == flight.AIRLINE && flight.DAY_OF_WEEK == 2 && flight.DAY >= from && flight.DAY <= to) {
                    const day = this.checkDays(Number(flight.DAY) -1);
                    airline.data[day] = airline.data[day] + 1;
                }
                if (airline.label == flight.AIRLINE && flight.DAY_OF_WEEK == 3 && flight.DAY >= from && flight.DAY <= to) {
                    const day = this.checkDays(Number(flight.DAY) -1);
                    airline.data[day] = airline.data[day] + 1;
                }
                if (airline.label == flight.AIRLINE && flight.DAY_OF_WEEK == 4 && flight.DAY >= from && flight.DAY <= to) {
                    const day = this.checkDays(Number(flight.DAY) -1);
                    airline.data[day] = airline.data[day] + 1;
                }
                if (airline.label == flight.AIRLINE && flight.DAY_OF_WEEK == 5 && flight.DAY >= from && flight.DAY <= to) {
                    const day = this.checkDays(Number(flight.DAY) -1);
                    airline.data[day] = airline.data[day] + 1;
                }
                if (airline.label == flight.AIRLINE && flight.DAY_OF_WEEK == 6 && flight.DAY >= from && flight.DAY <= to) {
                    const day = this.checkDays(Number(flight.DAY) -1);
                    airline.data[day] = airline.data[day] + 1;
                }
                if (airline.label == flight.AIRLINE && flight.DAY_OF_WEEK == 7 && flight.DAY >= from && flight.DAY <= to) {
                    const day = this.checkDays(Number(flight.DAY) -1);
                    airline.data[day] = airline.data[day] + 1;
                }
             //   airline.data = this.moveZeros(airline.data);
                lastDay = flight.DAY;
            
            });
            if( flight.DAY_OF_WEEK == 1 && flight.DAY >= from && flight.DAY <= to){
                const day = this.checkDays(Number(flight.DAY) -1);
                total[day] =  total[day] +1;
            }
            if( flight.DAY_OF_WEEK == 2 && flight.DAY >= from && flight.DAY <= to){
                const day = this.checkDays(Number(flight.DAY) -1);
                total[day] =  total[day] +1;
            }
            if( flight.DAY_OF_WEEK == 3 && flight.DAY >= from && flight.DAY <= to){
                const day = this.checkDays(Number(flight.DAY) -1);
                total[day] =  total[day] +1;
            }
            if( flight.DAY_OF_WEEK == 4 && flight.DAY >= from && flight.DAY <= to){
                const day = this.checkDays(Number(flight.DAY) -1);
                total[day] =  total[day] +1;
            }
            if( flight.DAY_OF_WEEK == 5 && flight.DAY >= from && flight.DAY <= to){
                const day = this.checkDays(Number(flight.DAY) -1);
                total[day] =  total[day] +1;
            }
            if( flight.DAY_OF_WEEK == 6 && flight.DAY >= from && flight.DAY <= to){
                const day = this.checkDays(Number(flight.DAY) -1);
                total[day] =  total[day] +1;
            }
            if( flight.DAY_OF_WEEK == 7 && flight.DAY >= from && flight.DAY <= to){
                const day = this.checkDays(Number(flight.DAY) -1);
                total[day] =  total[day] +1;
            }
        });

        firstDay = Number(this.sortedData[month].data[0].DAY_OF_WEEK) - 1;
        this.labels = [this.days[firstDay] + " " + this.months[month] + " " + from, this.days[this.checkDays(firstDay + 1)] + " " + this.months[month] + " " + this.addNum(1, from),
        this.days[this.checkDays(firstDay + 2)] + " " + this.months[month] + " " + this.addNum(2, from), this.days[this.checkDays(firstDay + 3)] + " " + this.months[month] + " " + this.addNum(3, from),
        this.days[this.checkDays(firstDay + 4)] + " " + this.months[month] + " " + this.addNum(4, from), this.days[this.checkDays(firstDay + 5)] + " " + this.months[month] + " " + this.addNum(5, from),
        this.days[this.checkDays(firstDay + 6)] + " " + this.months[month] + " " + this.addNum(6, from)];

        if(this.getLastDay(month) == 30 && from == 29 && to ==30){
            this.labels = [this.days[firstDay] + " " + this.months[month] + " " + from, this.days[this.checkDays(firstDay + 1)] + " " + this.months[month] + " " + this.addNum(1, from)];
        }

        if(this.getLastDay(month) == 31  && from == 29 && to ==31){
            this.labels = [this.days[firstDay] + " " + this.months[month] + " " + from, this.days[this.checkDays(firstDay + 1)] + " " + this.months[month] + " " + this.addNum(1, from),
            this.days[this.checkDays(firstDay + 2)] + " " + this.months[month] + " " + this.addNum(2, from)];
        }
        total.forEach((data,index)=>{
            total[index] = {
              name : this.days[this.checkDays(index + firstDay)],
              size : data,
            }
          })


        let opacity = 0.8;
        if (chartType == 'line') {
            opacity = 0.3;
        }

        this.airlines.forEach((airline) => {
            const color = this.genColor();
            airline.backgroundColor = NbColorHelper.hexToRgbA(color, opacity);
            airline.borderColor = color;
            airline.pointRadius = 3;
        });

        const weeklyData: chartData = {
            data: this.airlines,
            labels: this.labels,
            lastDay: lastDay, 
            total: total
        }
        return weeklyData;
        // const airlines
    }

    moveZeros(a) {

        let i = 0;

        for (let j = 0; j < a.Length; j++)
            if (a[j] != 0)
                a[i++] = a[j];

        while (i < a.Length)
            a[i++] = 0;
        return a;
    }

    checkDays(num) {
 
        if(num >6 && num < 14 ){
            num = num - 7;
        }
        if(num >13 && num < 21 ){
            num = num - 14;
        }
        if(num >20 && num < 28 ){
            num = num - 21;
        }
        if(num >27 && num < 32 ){
            num = num - 28;
        }

        return num;
    }
    addNum(num, from) {
        let day = num + +from;
        return day;
    }

    genColor() {
        return '#' + ("000000" + Math.random().toString(16).slice(2, 8).toUpperCase()).slice(-6);
    }
    sortMonths(data, months) {
        data.forEach((element: Flight) => {
            if (Number(element.MONTH) == 1) {
                months[element.MONTH - 1].data.push(element);
            }
            if (Number(element.MONTH) == 2) {
                months[element.MONTH - 1].data.push(element);
            }
            if (Number(element.MONTH) == 3) {
                months[element.MONTH - 1].data.push(element);
            }
            if (Number(element.MONTH) == 4) {
                months[element.MONTH - 1].data.push(element);
            }
            if (Number(element.MONTH) == 5) {
                months[element.MONTH - 1].data.push(element);
            }
            if (Number(element.MONTH) == 6) {
                months[element.MONTH - 1].data.push(element);
            }
            if (Number(element.MONTH) == 7) {
                months[element.MONTH - 1].data.push(element);
            }
            if (Number(element.MONTH) == 8) {
                months[element.MONTH - 1].data.push(element);
            }
            if (Number(element.MONTH) == 9) {
                months[element.MONTH - 1].data.push(element);
            }
            if (Number(element.MONTH) == 10) {
                months[element.MONTH - 1].data.push(element);
            }
            if (Number(element.MONTH) == 11) {
                months[element.MONTH - 1].data.push(element);
            }
            if (Number(element.MONTH) == 12) {
                months[element.MONTH - 1].data.push(element);
            }
        });
        return months;
    }

    initMonths() {
        let months = [
            {
                name: "January",
                data: new Array(),
            },
            {
                name: "February",
                data: new Array(),
            },
            {
                name: "March",
                data: new Array(),
            },
            {
                name: "April",
                data: new Array(),
            },
            {
                name: "May",
                data: new Array(),
            }, {
                name: "June",
                data: new Array(),
            }
            , {
                name: "July",
                data: new Array(),
            }
            , {
                name: "August",
                data: new Array(),
            }
            , {
                name: "September",
                data: new Array(),
            }
            , {
                name: "October",
                data: new Array(),
            }
            , {
                name: "November",
                data: new Array(),
            }
            , {
                name: "December",
                data: new Array(),
            }
        ];
        return months;
    }

}