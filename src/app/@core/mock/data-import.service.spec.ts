import {DataImportService} from './data-import.service';
import data from './flights_mini.json';

describe('DataImportService', () => {
  let service;

  beforeEach(() => {
    service = new DataImportService();
  });

  it('should run #getAirlines()', async () => {
    const result = service.getAirlines(data);
    expect(result.length).toEqual(11);

  });

  it('should run #getDataByMonth()', async () => {
    const result = service.getDataByMonth(2, data);
    expect(result.length).toBeLessThanOrEqual(0);
    const result2 = service.getDataByMonth(1, data);
    expect(result2.length).toBeGreaterThan(1);

  });

  it('should run #getAllData()', async () => {
    const result = service.getAllData(data);
    expect(result.data.length).toBeGreaterThan(1);
    expect(result.labels.length).toEqual(12);
  });

  it('should run #checkDays()', async () => {
    const result = service.checkDays(13);
    expect(result).toEqual(6);
  });

  it('should run #addNum()', async () => {
    const result = service.addNum(2, "8");
   expect(result).toEqual(10);

  });

  it('should run #genColor()', async () => {
    const result = service.genColor();
    expect(result).toBeDefined();

  });

 
});