import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by">Created by Caleb Tony-Enwin</span>
   
  `,
})
export class FooterComponent {
}
