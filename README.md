Caleb Tony-Enwin

Kinaxis Exercise:

Question answered:How many flights are there each day of the week? From which air carriers?
Built with typescript,angular 2+

BUILD configurations
run npm i

RUN configurations
run npm start
upload flights_2015_sample.csv



Run Tests
ng test

compile and deploy (for project owners)
git add .
git commit -m "add tests"
git push -u origin master or --no-verify
npm run build -- --prod --aot

firebase deploy

tests json is called flights_mini.json -- it is a smaller file

thanks guys
